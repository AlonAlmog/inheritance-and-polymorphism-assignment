# Inheritance and Polymorphism Assignment

In this assignment, given an outdated system, we were assigned to create new classes that would inherit from
the older classes, in order to create new object that would befit the new required system.

Some operations on those new object would be done through both inherited methods, and in some cases by using polymorphism 
along with exception handling.
