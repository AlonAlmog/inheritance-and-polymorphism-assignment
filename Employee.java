
public abstract class Employee {
	private final String firstName;
	private final String lastName;
	private final String socialSecurityNumber;
	public final BirthDate birthDate;
	
	public Employee(String firstName, String lastName, String socialSecurityNumber, int day, int month, int year) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.socialSecurityNumber = socialSecurityNumber;
		this.birthDate = new BirthDate(day, month, year);
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}
	
	public BirthDate getBirthDate() {
		return birthDate;
	}
	
	@Override
	public String toString() {
		return String.format("%s %s%nsocial security number: %s%n%s", getFirstName(), getLastName(), getSocialSecurityNumber(), getBirthDate());
	}
	
	public abstract double earnings();
	
}
