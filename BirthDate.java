
public class BirthDate {
	private int day;
	private int month;
	private int year;
	
	public BirthDate(int day, int month, int year) {
		if(day < 0 || day > 31) {
			throw new IllegalArgumentException("day must be between 0 to 31 (including both numbers)");
		}
		
		if(month < 1 || month > 12) {
			throw new IllegalArgumentException("month must be between 1 to 12 (including both numbers)");
		}
		
		if(year < 1900 || month > 2021) {
			throw new IllegalArgumentException("year must be between 1900 to 2021 (including both numbers)");
		}
		
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public int getDay() {
		return day;
	}
	
	public int getMonth() {
		return month;
	}
	
	public int getYear() {
		return year;
	}
	
	
	@Override
	public String toString() {
		return String.format("date of birth: %02d %02d %d", day, month, year);
	}

}
