import java.util.Calendar;

public class PayrollSystemTest {

	public static void main(String[] args) {
		SalariedEmployee salariedEmployee = new SalariedEmployee("John", "Smith", "111-11-1111", 800.00, 2, 9, 1998);
		HourlyEmployee hourlyEmployee = new HourlyEmployee("Karen", "Price", "222-22-2222", 16.75, 40, 17, 11, 2000);
		CommissionEmployee commissionEmployee = new CommissionEmployee("Sue", "Jones", "333-33-3333", 10000, .06, 24, 7, 1990);
		BasePlusCommissionEmployee basePlusCommissionEmployee = new BasePlusCommissionEmployee("Bob", "Lewis", "444-44-4444", 5000, .04, 300, 9, 11, 2010);
		PieceWorker pieceWorker = new PieceWorker("David", "Buchinski", "555-55-5555", 100, 6.5, 23, 5, 1998);

		Calendar currentDate = Calendar.getInstance();

		Employee[] employees = new Employee[5];
		
		employees[0] = salariedEmployee;
		employees[1] = hourlyEmployee;
		employees[2] = commissionEmployee;
		employees[3] = basePlusCommissionEmployee;
		employees[4] = pieceWorker;
		
		System.out.printf("Employees processed polymorphically:%n%n");
		
		for(Employee currentEmployee : employees) {
			
			System.out.println(currentEmployee);
			
			/* checks if employee has birthday this month */
			if(currentEmployee.getBirthDate().getMonth() == (currentDate.get(Calendar.MONTH) + 1)) {
				
				/* checking what kind of employee is it, and adding $200 accordingly */
				if(currentEmployee instanceof SalariedEmployee) {
					SalariedEmployee employee = (SalariedEmployee)currentEmployee;
					employee.setWeeklySalary(employee.getWeeklySalary() + 200);
				}
				else if(currentEmployee instanceof HourlyEmployee) {
					HourlyEmployee employee = (HourlyEmployee)currentEmployee;
					
					if( employee.getHours() <= 40) {
						employee.setWage(employee.getWage() + (200.0 / employee.getHours()));
					} else {
						employee.setWage(employee.getWage() + (200.0 / (1.5 * employee.getHours() - 20.0)));
					}
				}
				else if(currentEmployee instanceof CommissionEmployee) {
					CommissionEmployee employee = (CommissionEmployee)currentEmployee;
					employee.setCommissionRate(employee.getCommissionRate() + (200.0 / employee.getGrossSales()));
				} 
				else if(currentEmployee instanceof BasePlusCommissionEmployee) {
					BasePlusCommissionEmployee employee =  (BasePlusCommissionEmployee)currentEmployee;
					employee.setBaseSalary(200.0 + employee.getBaseSalary());
				}
				else if(currentEmployee instanceof PieceWorker) {
					PieceWorker employee = (PieceWorker)currentEmployee;
					employee.setPieces(employee.getPieces() + (200.0 / employee.getPiecePrice()));
				}
				
				System.out.println("you get $200 extra this month, happy birthday!");
			}

			System.out.printf("earned $%,.2f%n%n", currentEmployee.earnings());
		}

	}

}
