
public class PieceWorker extends Employee{
	private double pieces;
	private double piecePrice;
	
	public PieceWorker(String firstName, String lastName, String socialSecurityNumber, double pieces, double piecePrice, int day, int month, int year) {
		super(firstName, lastName, socialSecurityNumber, day, month, year);
		
		if(pieces < 0.0) {
			throw new IllegalArgumentException("number of pieces must be >= 0.0");
		}
		
		if(piecePrice < 0.0) {
			throw new IllegalArgumentException("price per piece must be >= 0.0");
		}
		
		this.piecePrice = piecePrice;
		this.pieces = pieces;
	}
	
	public void setPieces(double pieces) {
		if(pieces < 0.0) {
			throw new IllegalArgumentException("number of pieces must be >= 0.0");
		}
		
		this.pieces = pieces;
	}
	
	public void setPiecePrice(double piecePrice) {
		if(piecePrice < 0.0) {
			throw new IllegalArgumentException("price per piece must be >= 0.0");
		}
		
		this.piecePrice = piecePrice;
	}
	
	public double getPieces() {
		return pieces;
	}
	
	public double getPiecePrice() {
		return piecePrice;
	}
	
	@Override
	public double earnings() {
		return getPieces() * getPiecePrice();
	}
	
	@Override
	public String toString() {
		return String.format("piece worker employee: %s%n%s %,.2f; %s $%,.2f", super.toString(), "number of pieces:", getPieces(), "piece price:", getPiecePrice());
	}

}
